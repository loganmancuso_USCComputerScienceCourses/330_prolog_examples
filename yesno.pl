yes_no(String) :- 
   split_words(String,Words),  % Get the list of words.
   yn(Words).                  % Use yn on the words.

yn([Verb|Rest]) :-
   Verb=is,                    % The first word must be "is".
   append(W1,W2,Rest),         % Break the rest into two parts.
   np(W1,Ref),                 % The first part must be an NP.
   np_or_pp(W2,Ref).           % The second part must be an NP or a PP.

np_or_pp(W,Ref) :- np(W,Ref).
np_or_pp(W,Ref) :- pp(W,Ref).
