% solution(A,B,C,D,E) holds if A,B,C,D,E are colors 
% that solve a map-coloring problem from the text.
solution(A,B,C,D,E) :-
   color(A), color(B), color(C), color(D), color(E), 
   \+ A=B, \+ A=C, \+ A=D, \+ A=E, \+ B=C, \+ C=D, \+ D=E.

% The three colors are these.
color(red).
color(white).
color(blue).
