solution(AB,BC,CD,DE,EF,FG,GH,HA,HI,IB,IJ,JC,JK,KD,KF) :-
    lvertex(AH,AB), arrow(BA,BI,BC), fork(CB,CD,CJ), 
    arrow(DC,DK,DE), lvertex(ED,EF), arrow(FE,FK,FG),
    lvertex(GF,GH), arrow(HG,HI,HA), fork(IH,IB,IJ),
    arrow(JK,JC,JI), fork(KD,KF,KJ),

    reverse(AB,BA), reverse(BC,CB), reverse(CD,DC), 
    reverse(DE,ED), reverse(EF,FE), reverse(FG,GF), 
    reverse(GH,HG), reverse(HA,AH), reverse(HI,IH), 
    reverse(IB,BI), reverse(IJ,JI), reverse(JC,CJ), 
    reverse(JK,KJ), reverse(KD,DK), reverse(KF,FK).

print_lshape_interpretation :- 
    solution(AB,BC,CD,DE,EF,FG,GH,HA,HI,IB,IJ,JC,JK,KD,KF), nl,
    write(AB), write(BC), write(CD), write(DE), write(EF), 
    write(FG), write(GH), write(HA), write(HI), write(IB), 
    write(IJ), write(JC), write(JK), write(KD), write(KF).
